/**
 * Package contains the Java Interface of Ipopt used in the
 * LEMPC / MHE joint paper.
 */
package org.coinor;
