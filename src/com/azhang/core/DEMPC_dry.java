package com.azhang.core;

import java.io.*;

public class DEMPC_dry {


    public static void main(String[] args) {
    /* Simulation parameters */
//    double tf = (double)2/24;     // simulation time
        double tf = 14;     // simulation time
        double h = (double) 1 / 24 / 600;          // Integration step: 0.1min
        double Delta = (double) 1 / 24 / 4;        // Sampling period: 15mins

        int nStates = 145;
        int nStates1 = 52;
        int nStates2 = 93;

        int mInputs = 2;
        int mOutputs = 2;
        int mInput_1 = 1;
        int mInput_2 = 1;
        int mOutput_1 = 1;
        int mOutput_2 = 1;


        double[] us = new double[mInputs];      // Steady-state input
        double[] yset = {2, 1};


        int Nu = 3;          // Control Prediction horizon
        int Nh = 0;          // terminal cost horizon
        int Np = 2;          // Prediction horizon
        double rho = 0;      // Omega_{rho} set
        double[] P = new double[nStates];// quadratic Lyapunov function
        P[0] = 1E-4;
        for (int i = 1; i < nStates; i++) {
            P[i] = 1;
        }
//--------------------------------------------------------------------------
        /* Variables used to calculate performance **/
        int tb;
        int tend;
        int tp;
        double td;
        if (tf == 14) {
            /*  tf = 14, evaluate last 7 days  **/
            tb = (int) (7 / Delta) - 1;         // begin time of evaluation 671
            tend = (int) (14 / Delta) - 1;      // end time of evaluation 1343
            tp = tend - tb + 1;            // 673 numbers from 671~1343
            td = 14 - 7;                   // the numbers of evaluation days, last 7 days
        } else {
            /* When tf is not 14, evaluate from 0 to tf **/
            tb = 0;
            tend = (int) (tf / Delta) - 1;
            tp = tend - tb + 1;
            td = tf;
        }
//=====================================================================================================================		
//x0: close loop steady states of 100 days, read form "x0.txt".		
        double[] x0 = new double[145];
        String line;
        int row = 0;
        try {

            BufferedReader in = new BufferedReader(new FileReader(".\\x0.txt"));
            while ((line = in.readLine()) != null) {
                x0[row] = Double.parseDouble(line);
                row++;
            }
            in.close();
        } catch (NumberFormatException | IOException e) {
            e.printStackTrace();
        }


//-------------------------------------------------------------------------------------------------------		  
//dry: real dynamic dry weather influent data (Z0_all, Q0_all) of two weeks, read from "Inf_dry_2006.txt".

        double[][] dry = new double[1345][15];
        String line1;
        int row1 = 0;
        try {

            BufferedReader in = new BufferedReader(new FileReader(".\\Inf_dry_2006.txt"));  //
            while ((line1 = in.readLine()) != null) {
                String[] temp = line1.split("\t");
                for (int j = 0; j < temp.length; j++) {
                    dry[row1][j] = Double.parseDouble(temp[j]);
                }
                row1++;
            }
            in.close();

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//	  for(int i=0;i<dry.length;i++){
//	   for(int j=0;j<dry[i].length;j++){
//	    System.out.print(dry[i][j]+"\t");
//	   }
//	   System.out.println();
//	  }
//----		  
//	  Z0_all = dry(:,2:14);    % dry(i,:) =[t Si Ss Xi Xs Xbh Xba Xp So Sno Snh Snd Xnd Salk Q0];
//	  Q0_all = dry(:,15);

        double[][] Z0_all = new double[1345][13];
        double[] Q0_all = new double[1345];

        for (int i = 0; i < Z0_all.length; i++) {
            for (int j = 0; j < Z0_all[i].length; j++) {
                Z0_all[i][j] = dry[i][j + 1];
            }
        }
        for (int i = 0; i < Q0_all.length; i++) {
            Q0_all[i] = dry[i][14];
        }


//Predicted Q0_P_all and Z0_P_all of dry weather:  read from "dry_P.txt".

        double[][] dry_P = new double[2016][14];   // 3 weeks
        String line2;
        int row2 = 0;
        try {

            BufferedReader in = new BufferedReader(new FileReader(".\\dry_P.txt"));
            while ((line2 = in.readLine()) != null) {
                String[] temp = line2.split("\t");
                for (int j = 0; j < temp.length; j++) {
                    dry_P[row2][j] = Double.parseDouble(temp[j]);
                }
                row2++;
            }
            in.close();

        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        double[][] Z0_P_all = new double[2016][13];
        double[] Q0_P_all = new double[2016];

        for (int i = 0; i < Z0_P_all.length; i++) {
            for (int j = 0; j < Z0_P_all[i].length; j++) {
                Z0_P_all[i][j] = dry_P[i][j];
            }
        }
        for (int i = 0; i < Q0_P_all.length; i++) {
            Q0_P_all[i] = dry_P[i][13];
        }



//=========================================================================================================================================
// Initialize    

        int sM = (int) (tf / Delta) + 1; // Total sampling times in simulations

        double[] Q1_all = new double[sM + 1];
        double[] Qe_all = new double[sM + 1];
        double[] Qf_all = new double[sM + 1];
        double[] Qw_all = new double[sM + 1];
        double[] Qr_all = new double[sM + 1];
        double[] Qa_all = new double[sM + 1];
        double[] KLa5_all = new double[sM + 1];

        double Qr = 18446;
        double Qw = 385;
        double Q0_stab = 18446;
        double Q0;
        double Q1;
        double Qe;
        double Qf;
        double[] Z0;
        double KLa5;
        double Qa;
        
	    	    
		/* State & input trajectory data */
        double[][] x = new double[sM + 1][nStates];
        //double[][] X = new double[sM+1][nStates];  // the state matrix of x1+x2	predicted states trajectory
        double[][] xt = new double[nStates][sM + 1];   // the transposed matrix of x
        double[][] y = new double[sM + 1][mOutputs];
        double[] eso = new double[sM + 1];
        double[] esno = new double[sM + 1];
        double[] u = new double[(sM + 1) * mInputs];
        double[][] dataForPlot = new double[sM + 1][nStates + mInputs];
        double[] t = new double[sM + 1];

		/*current state & input trajectory*/
        double[] uk = new double[mInputs];
        double[] uk1 = new double[mInput_1];
        double[] uk2 = new double[mInput_2];
        double[] uk_1 = new double[mInputs];

        double[] xk = new double[nStates];
        double[][] XX = new double[Nu][nStates]; // predicted subsystem state
        double[] xk1 = new double[nStates1]; // subsystem1 state
        double[] xk2 = new double[nStates2]; // subsystem2 state


        double[] yk_1 = new double[mOutputs];
        double[] yk1 = new double[mOutput_1];
        double[] yk2 = new double[mOutput_2];

        double[] Q0_P = new double[Nu + Nh];
        double[][] Z0_P = new double[Nu + Nh][13];

//--------------------------------------------------------------------------------------------------------			

        Subsystem1_Bsm1Model sub1 = new Subsystem1_Bsm1Model(h, Delta, yset, us, P);
        //TODO: Subsystem2_Bsm1Model sub2 = new Subsystem2_Bsm1Model(h,Delta,yset,us,P);
        Bsm1Model bsm = new Bsm1Model(h, Delta, yset, us, P);

        TerminalcostDEMPC_1 tcDEMPC1 = new TerminalcostDEMPC_1(Nu, Np, Nh, Delta, h, rho, xk1, yk_1, uk_1, sub1, Q0_P, Z0_P, XX);
        //TODO: TerminalcostDEMPC_2 tcDEMPC2 = new TerminalcostDEMPC_2 (Nu, Np, Nh, Delta, h, rho, xk2, yk_1, uk_1, sub2, Q0_P, Z0_P, XX);

        /* Record the initial condition */

        x[0] = x0;
        for (int i = 0; i < nStates; i++) {
            xt[i][0] = x0[i];  // transposed matrix of x
        }
        y[0][0] = x0[59];
        y[0][1] = x0[21];
        eso[0] = Math.abs(x0[59] - yset[0]);
        esno[0] = Math.abs(x0[21] - yset[1]);

        Qa_all[0] = 16485.6074;
        KLa5_all[0] = 131.65;
        uk_1[0] = KLa5_all[0];
        uk_1[1] = Qa_all[0];
        System.arraycopy(x[0], 0, dataForPlot[0], 0, nStates);

        t[0] = 0;


        //initialize predicted subsystem states
        for (int i = 0; i < Nu; i++) {

            System.arraycopy(x0, 0, XX[i], 0, nStates);

        }
//--------------------------------------------------------------------------------------------------------			

/* MAIN SIMULATION */


/* Process model */

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("Dempc_dry_14.txt"));

            for (int iter = 0; iter < sM; iter++) {

                System.out.println("\n Current time: " + iter * Delta + " day");

                /* read real influent data	*/
                Z0 = Z0_all[iter];
                Q0 = Q0_all[iter];

//--------------------------------------------------------------------------------------------			
//Get the predicted Q0_P and Z0_P at current instant: the first one is real data, the rest are predicted data					

                Q0_P[0] = Q0;
                Z0_P[0] = Z0;

                for (int i = 1; i < Nu + Nh; i++) {
                    Q0_P[i] = Q0_P_all[i + iter];
                    Z0_P[i] = Z0_P_all[i + iter];
                }


// Add the following block to get non-periodical EMPC, only current influent is used at a sampling instant.			
//		for (int i = 0; i <  Nu+Nh; i++) {
//			Q0_P[i] = Q0;
//			Z0_P[i] = Z0;
//			
//		}
//---------------------------------------------------------------------------------------------			


                /* state measurement */


                xk = x[iter];

                System.arraycopy(xk, 0, xk1, 0, nStates1);   //subsystem state
                System.arraycopy(xk, 52, xk2, 0, nStates2);
                //System.arraycopy(xk, 0, X[0], 0, nStates);  //update first prediction X(:,1)= xk

                System.arraycopy(x0, 0, XX[0], 0, nStates);


                if (iter == 0) {
                    yk_1 = y[iter];
                } else {
                    yk_1 = y[iter - 1];
                }

                System.arraycopy(yk_1, 0, yk1, 0, mOutput_1);
                System.arraycopy(yk_1, 1, yk2, 0, mOutput_2);


                /* controller evaluation*/

                uk1 = tcDEMPC1.ctrl(xk1, yk1, uk1, Nu, Np, Nh, Q0_P, Z0_P, XX);
                //TODO: uk2 = tcDEMPC2.ctrl(xk2, yk2, uk2, Nu, Np, Nh, Q0_P, Z0_P, Xk);

                if (uk[0] < 0) {
                    uk[0] = 0;
                }
                if (uk[0] > 240) {
                    uk[0] = 240;
                }
                if (uk[1] < 0) {
                    uk[1] = 0;
                }
                if (uk[1] > 5 * Q0_stab) {
                    uk[1] = 5 * Q0_stab;
                }
                System.out.println(iter);


                /* implement control input */
                xk = bsm.intNomModel(xk, uk, Delta, Q0, Z0); //nominal model

                /* record the data */
                // data used to calculate performance
                KLa5 = uk[0];
                Qa = uk[1];
                Qe = Q0 - Qw;
                Q1 = Q0 + Qa + Qr;
                Qf = Q1 - Qa;

                Q1_all[iter] = Q1;
                Qe_all[iter] = Qe;
                Qf_all[iter] = Qf;
                Qr_all[iter] = Qr;
                Qw_all[iter] = Qw;
                KLa5_all[iter] = KLa5;
                Qa_all[iter] = Qa;
                //	-----------
                for (int i = 0; i < nStates; i++) {
                    x[iter + 1][i] = xk[i];
                    xt[i][iter + 1] = xk[i];  // transposed matrix of x
                }

                y[iter + 1][0] = xk[59];
                y[iter + 1][1] = xk[21];

                eso[iter + 1] = Math.abs(xk[59] - yset[0]);
                esno[iter + 1] = Math.abs(xk[21] - yset[1]);

                System.arraycopy(uk, 0, u, iter * mInputs, mInputs);
                System.arraycopy(uk, 0, uk_1, 0, mInputs);

                System.arraycopy(x[iter + 1], 0, dataForPlot[iter + 1], 0, nStates);
                System.arraycopy(uk, 0, dataForPlot[iter], nStates, mInputs);

                t[iter + 1] = iter + 1;

                String dataline = t[iter] + "\t";
                for (int i = 0; i < nStates + mInputs; i++) {
                    dataline = dataline + dataForPlot[iter][i] + "\t";
                }

                out.write(dataline);
                out.newLine();
                out.flush();
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//just to make up the last input data
        System.arraycopy(uk, 0, dataForPlot[sM], nStates + 0, mInputs);


/*plot result*/

        int[] display21 = {59, 145, 21, 146};
//new NicePlot("So #5 and sno #2", dataForPlot, t, display21).plot();
        int[] display1 = {0, 13 * 1, 13 * 2, 13 * 3, 13 * 4};
        int[] display2 = {1, 13 * 1 + 1, 13 * 2 + 1, 13 * 3 + 1, 13 * 4 + 1};
        int[] display3 = {2, 13 * 1 + 2, 13 * 2 + 2, 13 * 3 + 2, 13 * 4 + 2};
        int[] display4 = {3, 13 * 1 + 3, 13 * 2 + 3, 13 * 3 + 3, 13 * 4 + 3};
        int[] display5 = {4, 13 * 1 + 4, 13 * 2 + 4, 13 * 3 + 4, 13 * 4 + 4};
        int[] display6 = {5, 13 * 1 + 5, 13 * 2 + 5, 13 * 3 + 5, 13 * 4 + 5};
        int[] display7 = {6, 13 * 1 + 6, 13 * 2 + 6, 13 * 3 + 6, 13 * 4 + 6};
        int[] display8 = {7, 13 * 1 + 7, 13 * 2 + 7, 13 * 3 + 7, 13 * 4 + 7};
        int[] display9 = {8, 13 * 1 + 8, 13 * 2 + 8, 13 * 3 + 8, 13 * 4 + 8};
        int[] display10 = {9, 13 * 1 + 9, 13 * 2 + 9, 13 * 3 + 9, 13 * 4 + 9};
        int[] display11 = {10, 13 * 1 + 10, 13 * 2 + 10, 13 * 3 + 10, 13 * 4 + 10};
        int[] display12 = {11, 13 * 1 + 11, 13 * 2 + 11, 13 * 3 + 11, 13 * 4 + 11};
        int[] display13 = {12, 13 * 1 + 12, 13 * 2 + 12, 13 * 3 + 12, 13 * 4 + 12};

//				new NicePlot("SI: reactor1-reactor5", dataForPlot, t, display1).plot();
//				new NicePlot("SS: reactor1-reactor5", dataForPlot, t, display2).plot();
//				new NicePlot("XI: reactor1-reactor5", dataForPlot, t, display3).plot();
//				new NicePlot("XS: reactor1-reactor5", dataForPlot, t, display4).plot();
//				new NicePlot("XBH: reactor1-reactor5", dataForPlot, t, display5).plot();
//				new NicePlot("XBA: reactor1-reactor5", dataForPlot, t, display6).plot();
//				new NicePlot("XP: reactor1-reactor5", dataForPlot, t, display7).plot();
//				new NicePlot("SO: reactor1-reactor5", dataForPlot, t, display8).plot();
//				new NicePlot("SNO: reactor1-reactor5", dataForPlot, t, display9).plot();
//				new NicePlot("SNH: reactor1-reactor5", dataForPlot, t, display10).plot();
//				new NicePlot("SND: reactor1-reactor5", dataForPlot, t, display11).plot();
//				new NicePlot("XND: reactor1-reactor5", dataForPlot, t, display12).plot();
//				new NicePlot("SALK: reactor1-reactor5", dataForPlot, t, display13).plot();


//=======================================================================================================
//Performance		
//Overall plant performance during 7 to 14 days  

// IAE and ISE
        double[] eso_e = new double[tp];
        double[] esno_e = new double[tp];
        System.arraycopy(eso, tb, eso_e, 0, tp);
        System.arraycopy(esno, tb, esno_e, 0, tp);

        double IAE_so = bsm.trapz(eso_e);
        double IAE_sno = bsm.trapz(esno_e);
        double ISE_so = bsm.trapz(bsm.dotm(eso_e, eso_e));
        double ISE_sno = bsm.trapz(bsm.dotm(esno_e, esno_e));

// other performance
        double[] XI5_e = new double[tp];
        double[] XS5_e = new double[tp];
        double[] XBH5_e = new double[tp];
        double[] XBA5_e = new double[tp];
        double[] XP5_e = new double[tp];
        double[] XND5_e = new double[tp];
        double[] Xf_e = new double[tp];
        double[] Xe_e = new double[tp];
        double[] XIe_e = new double[tp];
        double[] XSe_e = new double[tp];
        double[] XBHe_e = new double[tp];
        double[] XBAe_e = new double[tp];
        double[] XPe_e = new double[tp];
        double[] XNDe_e = new double[tp];
        double[] Xw_e = new double[tp];
        double[] XIw_e = new double[tp];
        double[] XSw_e = new double[tp];
        double[] XBHw_e = new double[tp];
        double[] XBAw_e = new double[tp];
        double[] XPw_e = new double[tp];
        double[] XNDw_e = new double[tp];
        double[] SIe_e = new double[tp];
        double[] SSe_e = new double[tp];
        double[] SOe_e = new double[tp];
        double[] SNOe_e = new double[tp];
        double[] SNHe_e = new double[tp];
        double[] SNDe_e = new double[tp];
        double[] SALKe_e = new double[tp];
        double[] SNHe = new double[tp];
        double[] TSSe = new double[tp];
        double[] SNkje = new double[tp];
        double[] Ntot = new double[tp];
        double[] CODe = new double[tp];
        double[] BOD5e = new double[tp];
        double[] EQ_f = new double[tp];
        double[] PE_f = new double[tp];
        double[] SNkj0 = new double[tp];
        double[] SS0 = new double[tp];
        double[] BOD50 = new double[tp];
        double[] COD0 = new double[tp];
        double[] IQ_f = new double[tp];

        double[] Qe_e = new double[tp];
        double[] Q0_e = new double[tp];
        double[] Qw_e = new double[tp];
        double[] Qr_e = new double[tp];
        double[] Qa_e = new double[tp];
        double[] KLa5_e = new double[tp];
        System.arraycopy(Qe_all, tb, Qe_e, 0, tp);
        System.arraycopy(Q0_all, tb, Q0_e, 0, tp);
        System.arraycopy(Qw_all, tb, Qw_e, 0, tp);
        System.arraycopy(Qr_all, tb, Qr_e, 0, tp);
        System.arraycopy(Qa_all, tb, Qa_e, 0, tp);
        System.arraycopy(KLa5_all, tb, KLa5_e, 0, tp);

        double iXB = 0.08;
        double iXP = 0.06;
        double fP = 0.08;
        double V1 = 1000;
        double V2 = 1000;
        double V3 = 1333;
        double V4 = 1333;
        double V5 = 1333;
        double SO_star = 8;
        double A = 1500;
        double zm = 0.4;
        double KLa3 = 240;
        double KLa4 = 240;

//  XI XS XBH XBA XP XND in reactor 5 for the last 7 days	

        System.arraycopy(xt[54], tb, XI5_e, 0, tp);
        System.arraycopy(xt[55], tb, XS5_e, 0, tp);
        System.arraycopy(xt[56], tb, XBH5_e, 0, tp);
        System.arraycopy(xt[57], tb, XBA5_e, 0, tp);
        System.arraycopy(xt[58], tb, XP5_e, 0, tp);
        System.arraycopy(xt[63], tb, XND5_e, 0, tp);
        for (int i = 0; i < XI5_e.length; i++) {
            Xf_e[i] = 0.75 * (XI5_e[i] + XS5_e[i] + XBH5_e[i] + XBA5_e[i] + XP5_e[i]);
        }

//  XI XS XBH XBA XP XND effluent (layer 10)

        System.arraycopy(xt[74], tb, Xe_e, 0, tp);
        for (int i = 0; i < Xe_e.length; i++) {
            XIe_e[i] = (Xe_e[i] / Xf_e[i]) * XI5_e[i];
            XSe_e[i] = (Xe_e[i] / Xf_e[i]) * XS5_e[i];
            XBHe_e[i] = (Xe_e[i] / Xf_e[i]) * XBH5_e[i];
            XBAe_e[i] = (Xe_e[i] / Xf_e[i]) * XBA5_e[i];
            XPe_e[i] = (Xe_e[i] / Xf_e[i]) * XP5_e[i];
            XNDe_e[i] = (Xe_e[i] / Xf_e[i]) * XND5_e[i];
        }

//  XI XS XBH XBA XP XND underflow (layer 1), Zu = Zr = Zw, Qu = Qr + Qw;

        System.arraycopy(xt[65], tb, Xw_e, 0, tp);
        for (int i = 0; i < Xw_e.length; i++) {
            XIw_e[i] = (Xw_e[i] / Xf_e[i]) * XI5_e[i];
            XSw_e[i] = (Xw_e[i] / Xf_e[i]) * XS5_e[i];
            XBHw_e[i] = (Xw_e[i] / Xf_e[i]) * XBH5_e[i];
            XBAw_e[i] = (Xw_e[i] / Xf_e[i]) * XBA5_e[i];
            XPw_e[i] = (Xw_e[i] / Xf_e[i]) * XP5_e[i];
            XNDw_e[i] = (Xw_e[i] / Xf_e[i]) * XND5_e[i];
        }

//  SI SS SO SNO SNH SND SALK effluent (layer 10)

        System.arraycopy(xt[84], tb, SIe_e, 0, tp);
        System.arraycopy(xt[94], tb, SSe_e, 0, tp);
        System.arraycopy(xt[104], tb, SOe_e, 0, tp);
        System.arraycopy(xt[114], tb, SNOe_e, 0, tp);
        System.arraycopy(xt[124], tb, SNHe_e, 0, tp);
        System.arraycopy(xt[134], tb, SNDe_e, 0, tp);
        System.arraycopy(xt[144], tb, SALKe_e, 0, tp);

        //  Effluent average concentrations
        //  SI SS XI XS XBH XBA XP SO SNO SNH SND XND SALK

        double ave_Qe = bsm.trapz(Qe_e) / td;
        double ave_SIe = (bsm.trapz(bsm.dotm(SIe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_SSe = (bsm.trapz(bsm.dotm(SSe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XIe = (bsm.trapz(bsm.dotm(XIe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XSe = (bsm.trapz(bsm.dotm(XSe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XBHe = (bsm.trapz(bsm.dotm(XBHe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XBAe = (bsm.trapz(bsm.dotm(XBAe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XPe = (bsm.trapz(bsm.dotm(XPe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_SOe = (bsm.trapz(bsm.dotm(SOe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_SNOe = (bsm.trapz(bsm.dotm(SNOe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_SNHe = (bsm.trapz(bsm.dotm(SNHe_e, Qe_e))) / (bsm.trapz(Qe_e));  // limit = 4 mg N/l
        double ave_SNDe = (bsm.trapz(bsm.dotm(SNDe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_XNDe = (bsm.trapz(bsm.dotm(XNDe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_SALKe = (bsm.trapz(bsm.dotm(SALKe_e, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_TSSe = 0.75 * (ave_XIe + ave_XSe + ave_XBHe + ave_XBAe + ave_XPe);  // limit = 30 mg SS/l

        for (int i = 0; i < SNHe_e.length; i++) {
            SNkje[i] = SNHe_e[i] + SNDe_e[i] + XNDe_e[i] + iXB * (XBHe_e[i] + XBAe_e[i]) + iXP * (XPe_e[i] + XIe_e[i]);
            Ntot[i] = SNkje[i] + SNOe_e[i];
            CODe[i] = SSe_e[i] + SIe_e[i] + XSe_e[i] + XIe_e[i] + XBHe_e[i] + XBAe_e[i] + XPe_e[i];
            BOD5e[i] = 0.25 * (SSe_e[i] + XSe_e[i] + (1 - fP) * (XBHe_e[i] + XBAe_e[i]));
        }

        double ave_SNkje = (bsm.trapz(bsm.dotm(SNkje, Qe_e))) / (bsm.trapz(Qe_e));
        double ave_Ntot = (bsm.trapz(bsm.dotm(Ntot, Qe_e))) / (bsm.trapz(Qe_e));  // limit = 18 mg COD/l
        double ave_CODe = (bsm.trapz(bsm.dotm(CODe, Qe_e))) / (bsm.trapz(Qe_e));  // limit = 100 mg COD/l
        double ave_BOD5e = (bsm.trapz(bsm.dotm(BOD5e, Qe_e))) / (bsm.trapz(Qe_e));  // limit = 10 mg COD/l

        //  Effluent average load
        //  % SI SS XI XS XBH XBA XP SO SNO SNH SND XND SALK

        double ave_SIel = (bsm.trapz(bsm.dotm(SIe_e, Qe_e))) / td / 1000;
        double ave_SSel = (bsm.trapz(bsm.dotm(SSe_e, Qe_e))) / td / 1000;
        double ave_XIel = (bsm.trapz(bsm.dotm(XIe_e, Qe_e))) / td / 1000;
        double ave_XSel = (bsm.trapz(bsm.dotm(XSe_e, Qe_e))) / td / 1000;
        double ave_XBHel = (bsm.trapz(bsm.dotm(XBHe_e, Qe_e))) / td / 1000;
        double ave_XBAel = (bsm.trapz(bsm.dotm(XBAe_e, Qe_e))) / td / 1000;
        double ave_XPel = (bsm.trapz(bsm.dotm(XPe_e, Qe_e))) / td / 1000;
        double ave_SOel = (bsm.trapz(bsm.dotm(SOe_e, Qe_e))) / td / 1000;
        double ave_SNOel = (bsm.trapz(bsm.dotm(SNOe_e, Qe_e))) / td / 1000;
        double ave_SNHel = (bsm.trapz(bsm.dotm(SNHe_e, Qe_e))) / td / 1000;
        double ave_SNDel = (bsm.trapz(bsm.dotm(SNDe_e, Qe_e))) / td / 1000;
        double ave_XNDel = (bsm.trapz(bsm.dotm(XNDe_e, Qe_e))) / td / 1000;
        double ave_SALKel = (bsm.trapz(bsm.dotm(SALKe_e, Qe_e))) / td / 1000;
        double ave_DELTASel = 0.75 * (ave_XIel + ave_XSel + ave_XBHel + ave_XBAel + ave_XPel);

        double ave_SNkjel = (bsm.trapz(bsm.dotm(SNkje, Qe_e))) / td / 1000;
        double ave_Ntotl = (bsm.trapz(bsm.dotm(Ntot, Qe_e))) / td / 1000;
        double ave_CODel = (bsm.trapz(bsm.dotm(CODe, Qe_e))) / td / 1000;
        double ave_BOD5el = (bsm.trapz(bsm.dotm(BOD5e, Qe_e))) / td / 1000;

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //  Effluent constraints for the five violation variables, 7 days
        //Ntot;         // limit = 18 mg COD/l
        //CODe;         // limit = 100 mg COD/l
        SNHe = SNHe_e;  // limit = 4 mg N/l
        TSSe = Xe_e;    // limit = 30 mg SS/l
        //BOD5e;        // limit = 10 mg COD/l
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
//  Effluent quality (EQ)  
        for (int i = 0; i < TSSe.length; i++) {
            EQ_f[i] = (2 * TSSe[i] + CODe[i] + 30 * SNkje[i] + 10 * (SNOe_e[i]) + 2 * BOD5e[i]) * Qe_e[i];
        }
        double EQ = bsm.trapz(EQ_f) / td / 1000;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
        //   Influent quality (IQ)
        //  1-Si 2-Ss 3-Xi 4-Xs 5-Xbh 6-Xba 7-Xp 8-So 9-Sno 10-Snh 11-Snd 12-Xnd 13-Salk

        for (int i = 0; i < Qe_e.length; i++) {
            SNkj0[i] = Z0_all[i + tb][9] + Z0_all[i + tb][10] + Z0_all[i + tb][11] + iXB * (Z0_all[i + tb][4] + Z0_all[i + tb][5]) + iXP * (Z0_all[i + tb][6] + Z0_all[i + tb][2]);
            SS0[i] = 0.75 * (Z0_all[i + tb][2] + Z0_all[i + tb][3] + Z0_all[i + tb][4] + Z0_all[i + tb][5] + Z0_all[i + tb][6]);
            BOD50[i] = 0.65 * (Z0_all[i + tb][1] + Z0_all[i + tb][3] + (1 - fP) * (Z0_all[i + tb][4] + Z0_all[i + tb][5]));
            COD0[i] = Z0_all[i + tb][1] + Z0_all[i + tb][0] + Z0_all[i + tb][3] + Z0_all[i + tb][2] + Z0_all[i + tb][4] + Z0_all[i + tb][5] + Z0_all[i + tb][6];
            IQ_f[i] = (2 * SS0[i] + COD0[i] + 30 * SNkj0[i] + 10 * (Z0_all[i + tb][8]) + 2 * BOD50[i]) * Q0_e[i];
        }
        double IQ = bsm.trapz(IQ_f) / td / 1000;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //  Sludge production to be disposed (SP): (kg.d^(-1))
        double[] TSSa = new double[tp];
        double[] TSSs = new double[tp];
        double[] TSS = new double[tp];

        for (int i = 0; i < TSS.length; i++) {
            TSSa[i] = 0.75 * (V1 * (xt[2][i + tb] + xt[3][i + tb] + xt[4][i + tb] + xt[5][i + tb] + xt[6][i + tb]) +
                    V2 * (xt[15][i + tb] + xt[16][i + tb] + xt[17][i + tb] + xt[18][i + tb] + xt[19][i + tb]) +
                    V3 * (xt[28][i + tb] + xt[29][i + tb] + xt[30][i + tb] + xt[31][i + tb] + xt[32][i + tb]) +
                    V4 * (xt[41][i + tb] + xt[42][i + tb] + xt[43][i + tb] + xt[44][i + tb] + xt[45][i + tb]) +
                    V5 * (xt[54][i + tb] + xt[55][i + tb] + xt[56][i + tb] + xt[57][i + tb] + xt[58][i + tb]));
            TSSs[i] = zm * A * (xt[65][i + tb] + xt[66][i + tb] + xt[67][i + tb] + xt[68][i + tb] + xt[69][i + tb] + xt[70][i + tb] + xt[71][i + tb] + xt[72][i + tb] + xt[73][i + tb] + xt[74][i + tb]);
            TSS[i] = TSSa[i] + TSSs[i];
        }

        double SP = (TSS[TSS.length - 1] - TSS[0] + bsm.trapz(bsm.dotm(Xw_e, Qw_e))) / td / 1000;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
//  Total sludge production (SPtotal) (kg.d^(-1))  
        double SPtotal = SP + bsm.trapz(bsm.dotm(Xe_e, Qe_e)) / td / 1000;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //   Aeration energy (AE), Pumping energy (PE): (kWh.d^(-1))
        //  supposing KLa3 = KLa4 =240 not change
        for (int i = 0; i < Qa_e.length; i++) {
            PE_f[i] = 0.004 * Qa_e[i] + 0.008 * Qr_e[i] + 0.05 * Qw_e[i];
        }
        double PE = bsm.trapz(PE_f) / td;
        double AE = SO_star / td / 1.8 / 1000 * (0 + 0 + V3 * KLa3 * td + V4 * KLa4 * td + bsm.trapz(KLa5_e) * V5);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //  Mixing energy (ME): (kWh.d^(-1))
        //  supposing KLa3 = KLa4 =240 not change
        double[] ME_5 = new double[tp];
        for (int i = 0; i < ME_5.length; i++) {
            if (KLa5_e[i] < 20) {
                ME_5[i] = 0.005 * V5;
            } else {
                ME_5[i] = 0;
            }
        }

        double ME = (double) 24 * (0.005 * V1 * td + 0.005 * V2 * td + 0 + 0 + bsm.trapz(ME_5)) / td;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//    consumption of external carbon source (EC) 
        double EC = 0;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Overall Cost Index (OCI) 
        double OCI = AE + PE + 5 * SP + 3 * EC + ME;


        System.out.println("Performance");
        System.out.println("-----------------------------------------");
        System.out.println("IQ = " + IQ + "  kg/d");
        System.out.println("EQ = " + EQ + "  kg/d");
        System.out.println("-----------------------------------------");
        System.out.println("SP = " + SP + "  kg/d");
        System.out.println("SPtotal = " + SPtotal + "  kg/d");
        System.out.println("AE = " + AE + "  kwh/d");
        System.out.println("PE = " + PE + "  kwh/d");
        System.out.println("ME = " + ME + "  kwh/d");
        System.out.println("OCI = " + OCI + " (OCI = AE + PE + 5*SP + ME)");
        System.out.println("-----------------------------------------");
        System.out.println("ave_Ntot = " + ave_Ntot + "  mg COD/l    (limit = 18 mg COD/l)");
        System.out.println("ave_CODe = " + ave_CODe + "  mg COD/l   (limit = 100 mg COD/l)");
        System.out.println("ave_SNHe = " + ave_SNHe + "  mg N/l      (limit = 4 mg N/l)");
        System.out.println("ave_TSSe = " + ave_TSSe + "  mg SS/l    (limit = 30 mg SS/l)");
        System.out.println("ave_BOD5e= " + ave_BOD5e + "  mg COD/l   (limit = 10 mg COD/l)");
        System.out.println("-----------------------------------------");
        System.out.println("IAE_so = " + IAE_so);
        System.out.println("ISE_so = " + ISE_so);
        System.out.println("IAE_sno = " + IAE_sno);
        System.out.println("ISE_sno = " + ISE_sno);
//=============================================================================================================

        try {
            BufferedWriter out1 = new BufferedWriter(new FileWriter("empc_dry_Ntot.txt"));
//    Ntot;  limit = 18 mg COD/l 
            for (int i = 0; i < tp; i++) {
                out1.write(Ntot[i] + "\t");
            }
            out1.write("\r\n");

            //   CODe;  limit = 100 mg COD/l
            for (int i = 0; i < tp; i++) {
                out1.write(CODe[i] + "\t");
            }
            out1.write("\r\n");

            //   SNHe;  limit = 4 mg N/l
            for (int i = 0; i < tp; i++) {
                out1.write(SNHe[i] + "\t");
            }
            out1.write("\r\n");

            //   TSSe;  limit = 30 mg SS/l
            for (int i = 0; i < tp; i++) {
                out1.write(TSSe[i] + "\t");
            }
            out1.write("\r\n");

            //   BOD5e;  limit = 10 mg COD/l
            for (int i = 0; i < tp; i++) {
                out1.write(BOD5e[i] + "\t");
            }
            out1.write("\r\n");

            out1.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
