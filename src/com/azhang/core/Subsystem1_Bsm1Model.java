package com.azhang.core;

public class Subsystem1_Bsm1Model {
	/** steady state */
    public final double[] yset;   
    public final double[] us; 
    public final double[] P; // quadratic Lyapunov function 
    
 // System parameters
    public final static double YA = 0.24; 
    public final static double YH = 0.67; 
    public final static double fP = 0.08; 
    public final static double iXB = 0.08; 
    public final static double iXP = 0.06;
    public final static double muH = 4.0;
    public final static double KS = 10.0;
    public final static double KOH = 0.2;
    public final static double KNO = 0.5; 
    public final static double bH = 0.3;  
    public final static double etag = 0.8; 
    public final static double etah = 0.8;
    public final static double kh = 3.0;
    public final static double KX = 0.1;
    public final static double muA = 0.5;
    public final static double KNH = 1.0;  
    public final static double bA = 0.05; 
    public final static double KOA = 0.4; 
    public final static double ka = 0.05;

//  Secondary settler: table 4
    public final static double v0_pie = 250; 
    public final static double v0 = 474; 
    public final static double rh = 0.000576; 
    public final static double rp = 0.00286;
    public final static double fns = 0.00228;
     
//  Else
    public final static double V1 = 1000; 
    public final static double V2 = 1000;
    public final static double V3 = 1333;
    public final static double V4 = 1333;
    public final static double V5 = 1333;
    public final static double SO_star = 8; 
    public final static double A = 1500;
    public final static double Xt = 3000;
    public final static double zm = 0.4;            
           
    public final static double KLa3 = 240;
    public final static double KLa4 = 240; 
    public final static double TSS0 = 2.4693e+07; 
    
    
 
    /** Number of states */
    public int nStates = 52;
    
    /** Number of inputs */
    public int mInputs = 1; 
    public int mOutputs = 1;
    
//    public double[] uk_1 = new double[mInputs]; // store last input, used for penalizing the incremental 
    /** Explicit Euler integration step */
    public double h;
    public double Delta;
    
    public Subsystem1_Bsm1Model(double h, double Delta, double[] yset, double[] us, double[] P ){
    	this.h = h;
    	this.Delta = Delta;
       	this.yset = yset;
    	this.us = us;
    	this.P = P;
    	
    }
   
 /*   
    public void setQ0(double Q0, double[] Z0){
    	this.Q0 = Q0;
    	this.Z0 = Z0;
    }
 */
    
//Bsm1 model
    public double[] nomVectorField(double[] x, double[] Xk, double[] u, double Q0, double[] Z0) {
    	
    double Qa = u[0];	
    
    double Qr = 18446;
	double Qw = 385;
	double Q1 = Q0 + Qa + Qr;
	double Qe = Q0 - Qw;
	double Qf = Q1 - Qa;
    double Q2 = Q1; 
	double Q3 = Q2; 
	double Q4 = Q3; 
    double Q5 = Q4;

	
    double[][] Z = new double [5][13];
    double[][] Z_temp = new double [5][13];
    double[][] ZS = new double [10][8];
    double[][] rho = new double [5][8];
    double[][] r = new double [5][13];
    double[] Zr = new double [13];
    double[] X = new double [10];
    double[] expfun = new double [10];
    double[] vs = new double [10];
    double[] Js = new double [10];
    double[] Jclar = new double [10];
    double Xf;  double Xr;  double Xmin;  double vdn;  double vup;
    double[] f = new double[nStates];
	
	  for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 13; j++) {
			Z[i][j] = x[(i)*13 + j];
		}
	}
	  
		

		for (int j = 0; j < 13; j++) {
			Z[4][j] = Xk[4*13 + j];
		}

	  
//	  for (int i = 0; i < Z.length; i++) {
//		for (int j = 0; j < Z[i].length; j++) {
//			System.out.print(Z[i][j]+" ");
//		}
//		System.out.println();
//	}
	  
	  for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 13; j++) {
			if (Z[i][j]<0) {
		       Z_temp[i][j] = 0;	
			}else {
			   Z_temp[i][j] = Z[i][j];
			}
		}
	}
//	  for (int i = 0; i < Z_temp.length; i++) {
//		for (int j = 0; j < Z_temp[i].length; j++) {
//			System.out.print(Z_temp[i][j]+" ");
//		}
//		System.out.println();
//	}
//	  

for (int i = 0; i < 8; i++) {
	for (int j = 0; j < 10; j++) {
		ZS[j][i] = Xk[i*10 + j + 65];
	}	
}

//  for (int i = 0; i < ZS.length; i++) {
//	for (int j = 0; j < ZS[i].length; j++) {
//		System.out.print(ZS[i][j]+" ");
//	}
//	System.out.println();
//}

//Bioreactor: function 1-65
//Eight basic processes 

for (int i = 0; i < 5; i++) {
	rho[i][0] = muH * (Z_temp[i][1]/(KS + Z_temp[i][1])) * (Z_temp[i][7]/(KOH + Z_temp[i][7])) * Z_temp[i][4];
	rho[i][1] = muH * (Z_temp[i][1]/(KS + Z_temp[i][1])) * (KOH/(KOH + Z_temp[i][7])) * (Z_temp[i][8]/(KNO + Z_temp[i][8])) *etag*Z_temp[i][4];
	rho[i][2] = muA * (Z_temp[i][9]/(KNH +Z_temp[i][9])) * (Z_temp[i][7]/(KOA +Z_temp[i][7])) * Z_temp[i][5];
	rho[i][3] = bH * Z_temp[i][4];
	rho[i][4] = bA * Z_temp[i][5];
	rho[i][5] = ka * Z_temp[i][10] * Z_temp[i][4];
	rho[i][6] = kh * (Z_temp[i][3]/Z_temp[i][4])/(KX +(Z_temp[i][3]/Z_temp[i][4])) * ( (Z_temp[i][7]/(KOH + Z_temp[i][7])) + etah* (KOH/(KOH + Z_temp[i][7])) * (Z_temp[i][8] /(KNO + Z_temp[i][8])) ) * Z_temp[i][4];
	rho[i][7] = kh * (Z_temp[i][3]/Z_temp[i][4])/(KX +(Z_temp[i][3]/Z_temp[i][4])) * ( (Z_temp[i][7]/(KOH + Z_temp[i][7])) + etah* (KOH/(KOH + Z_temp[i][7])) * (Z_temp[i][8] /(KNO + Z_temp[i][8])) ) * Z_temp[i][4] *(Z_temp[i][11]/Z_temp[i][3]);

}
//  for (int i = 0; i < rho.length; i++) {
//	for (int j = 0; j < rho[i].length; j++) {
//		System.out.print(rho[i][j]+" ");
//		
//	}
//	System.out.println();
//}

for (int j = 0; j < 5; j++) {
	r[j][0] = 0;
	r[j][1] = -1/YH * rho[j][0] - 1/YH * rho[j][1] + rho[j][6];
	r[j][2] = 0;
	r[j][3] = (1-fP) * rho[j][3] + (1-fP) * rho[j][4] - rho[j][6];
	r[j][4] = rho[j][0] + rho[j][1] - rho[j][3];
	r[j][5] = rho[j][2] - rho[j][4];
	r[j][6] = fP * rho[j][3] + fP * rho[j][4];
	r[j][7] = -(1-YH)/YH * rho[j][0] - (4.57-YA)/YA * rho[j][2];
	r[j][8] = -(1-YH)/ (2.86*YH) *rho[j][1] + 1/YA * rho [j][2];
	r[j][9] = - iXB * rho[j][0] - iXB * rho[j][1] - (iXB + 1/YA) * rho[j][2] + rho[j][5];
	r[j][10] = - rho[j][5] + rho[j][7];
	r[j][11] = (iXB - fP * iXP) * rho[j][3] + (iXB - fP * iXP) * rho[j][4] - rho[j][7];
	r[j][12] = - iXB/14 * rho[j][0] + ( (1-YH)/(14*2.86* YH) - iXB/14 )* rho[j][1] - ( iXB/14 + 1/(7*YA) )* rho[j][2] +  rho[j][5]/14;
}

//  for (int i = 0; i < r.length; i++) {
//	for (int j = 0; j < r[i].length; j++) {
//		System.out.print(r[i][j]+" ");	
//	}
//	System.out.println();
//}
  
  Zr[0] = ZS[0][1];  Zr[1] = ZS[0][2];  Zr[7] = ZS[0][3];  Zr[8] = ZS[0][4];  Zr[9] =  ZS[0][5];  Zr[10] = ZS[0][6];  Zr[12] =  ZS[0][7];         

  Xf = 0.75 * ( Z[4][2] + Z[4][3] + Z[4][4] + Z[4][5] + Z[4][6] );
  Xr = ZS[0][0];
  Zr[2] = (Xr/Xf)* Z[4][2];
  Zr[3] = (Xr/Xf)* Z[4][3];
  Zr[4] = (Xr/Xf)* Z[4][4];
  Zr[5] = (Xr/Xf)* Z[4][5];
  Zr[6] = (Xr/Xf)* Z[4][6];
  Zr[11] = (Xr/Xf)* Z[4][11];

//for (int j = 0; j < Zr.length; j++) {
//	System.out.println(Zr[j]);
//}

// Bioreactor 1
f[0] = 1/V1 * ( Qa*Z[4][0] + Qr*Zr[0] + Q0*Z0[0] + r[0][0]*V1 -  Q1*Z[0][0] );  // SI
f[1] = 1/V1 * ( Qa*Z[4][1] + Qr*Zr[1] + Q0*Z0[1] + r[0][1]*V1 -  Q1*Z[0][1] );  // SS
f[2] = 1/V1 * ( Qa*Z[4][2] + Qr*Zr[2] + Q0*Z0[2] + r[0][2]*V1 -  Q1*Z[0][2] );  // XI
f[3] = 1/V1 * ( Qa*Z[4][3] + Qr*Zr[3] + Q0*Z0[3] + r[0][3]*V1 -  Q1*Z[0][3] );  // XS
f[4] = 1/V1 * ( Qa*Z[4][4] + Qr*Zr[4] + Q0*Z0[4] + r[0][4]*V1 -  Q1*Z[0][4] );  // XBH
f[5] = 1/V1 * ( Qa*Z[4][5] + Qr*Zr[5] + Q0*Z0[5] + r[0][5]*V1 -  Q1*Z[0][5] );  // XBA
f[6] = 1/V1 * ( Qa*Z[4][6] + Qr*Zr[6] + Q0*Z0[6] + r[0][6]*V1 -  Q1*Z[0][6] );  // XP
f[7] = 1/V1 * ( Qa*Z[4][7] + Qr*Zr[7] + Q0*Z0[7] + r[0][7]*V1 -  Q1*Z[0][7] );  // SO
f[8] = 1/V1 * ( Qa*Z[4][8] + Qr*Zr[8] + Q0*Z0[8] + r[0][8]*V1 -  Q1*Z[0][8] );  // SNO
f[9] = 1/V1 * ( Qa*Z[4][9] + Qr*Zr[9] + Q0*Z0[9] + r[0][9]*V1 -  Q1*Z[0][9] );  // SNH
f[10] = 1/V1 * ( Qa*Z[4][10] + Qr*Zr[10] + Q0*Z0[10] + r[0][10]*V1 -  Q1*Z[0][10] );  // SND
f[11] = 1/V1 * ( Qa*Z[4][11] + Qr*Zr[11] + Q0*Z0[11] + r[0][11]*V1 -  Q1*Z[0][11] );  // XND
f[12] = 1/V1 * ( Qa*Z[4][12] + Qr*Zr[12] + Q0*Z0[12] + r[0][12]*V1 -  Q1*Z[0][12] );  // SALK
 
// Bioreactor 2
f[13] = 1/V2 * ( Q1*Z[0][0] + r[1][0]*V2 - Q2*Z[1][0] );
f[14] = 1/V2 * ( Q1*Z[0][1] + r[1][1]*V2 - Q2*Z[1][1] );
f[15] = 1/V2 * ( Q1*Z[0][2] + r[1][2]*V2 - Q2*Z[1][2] );
f[16] = 1/V2 * ( Q1*Z[0][3] + r[1][3]*V2 - Q2*Z[1][3] );
f[17] = 1/V2 * ( Q1*Z[0][4] + r[1][4]*V2 - Q2*Z[1][4] );
f[18] = 1/V2 * ( Q1*Z[0][5] + r[1][5]*V2 - Q2*Z[1][5] );
f[19] = 1/V2 * ( Q1*Z[0][6] + r[1][6]*V2 - Q2*Z[1][6] );
f[20] = 1/V2 * ( Q1*Z[0][7] + r[1][7]*V2 - Q2*Z[1][7] );
f[21] = 1/V2 * ( Q1*Z[0][8] + r[1][8]*V2 - Q2*Z[1][8] );
f[22] = 1/V2 * ( Q1*Z[0][9] + r[1][9]*V2 - Q2*Z[1][9] );
f[23] = 1/V2 * ( Q1*Z[0][10] + r[1][10]*V2 - Q2*Z[1][10] );
f[24] = 1/V2 * ( Q1*Z[0][11] + r[1][11]*V2 - Q2*Z[1][11] );
f[25] = 1/V2 * ( Q1*Z[0][12] + r[1][12]*V2 - Q2*Z[1][12] );
 
// Bioreactor 3
f[26] = 1/V3 * ( Q2*Z[1][0] + r[2][0]*V3 - Q3*Z[2][0] );
f[27] = 1/V3 * ( Q2*Z[1][1] + r[2][1]*V3 - Q3*Z[2][1] );
f[28] = 1/V3 * ( Q2*Z[1][2] + r[2][2]*V3 - Q3*Z[2][2] );
f[29] = 1/V3 * ( Q2*Z[1][3] + r[2][3]*V3 - Q3*Z[2][3] );
f[30] = 1/V3 * ( Q2*Z[1][4] + r[2][4]*V3 - Q3*Z[2][4] );
f[31] = 1/V3 * ( Q2*Z[1][5] + r[2][5]*V3 - Q3*Z[2][5] );
f[32] = 1/V3 * ( Q2*Z[1][6] + r[2][6]*V3 - Q3*Z[2][6] ); 
f[33] = 1/V3 * ( Q2*Z[1][7] + r[2][7]*V3 - Q3*Z[2][7] + KLa3*V3*(SO_star - Z[2][7]) );
f[34] = 1/V3 * ( Q2*Z[1][8] + r[2][8]*V3 - Q3*Z[2][8] );
f[35] = 1/V3 * ( Q2*Z[1][9] + r[2][9]*V3 - Q3*Z[2][9] );
f[36] = 1/V3 * ( Q2*Z[1][10] + r[2][10]*V3 - Q3*Z[2][10] );
f[37] = 1/V3 * ( Q2*Z[1][11] + r[2][11]*V3 - Q3*Z[2][11] );
f[38] = 1/V3 * ( Q2*Z[1][12] + r[2][12]*V3 - Q3*Z[2][12] );
 
// Bioreactor 4
f[39] = 1/V4 * ( Q3*Z[2][0] + r[3][0]*V4 - Q4*Z[3][0] );
f[40] = 1/V4 * ( Q3*Z[2][1] + r[3][1]*V4 - Q4*Z[3][1] );
f[41] = 1/V4 * ( Q3*Z[2][2] + r[3][2]*V4 - Q4*Z[3][2] );
f[42] = 1/V4 * ( Q3*Z[2][3] + r[3][3]*V4 - Q4*Z[3][3] );
f[43] = 1/V4 * ( Q3*Z[2][4] + r[3][4]*V4 - Q4*Z[3][4] );
f[44] = 1/V4 * ( Q3*Z[2][5] + r[3][5]*V4 - Q4*Z[3][5] );
f[45] = 1/V4 * ( Q3*Z[2][6] + r[3][6]*V4 - Q4*Z[3][6] );
f[46] = 1/V4 * ( Q3*Z[2][7] + r[3][7]*V4 - Q4*Z[3][7] + KLa4*V4*(SO_star - Z[3][7]) );
f[47] = 1/V4 * ( Q3*Z[2][8] + r[3][8]*V4 - Q4*Z[3][8] );
f[48] = 1/V4 * ( Q3*Z[2][9] + r[3][9]*V4 - Q4*Z[3][9] );
f[49] = 1/V4 * ( Q3*Z[2][10] + r[3][10]*V4 - Q4*Z[3][10] );
f[50] = 1/V4 * ( Q3*Z[2][11] + r[3][11]*V4 - Q4*Z[3][11] );
f[51] = 1/V4 * ( Q3*Z[2][12] + r[3][12]*V4 - Q4*Z[3][12] );
 
/*
// Bioreactor 5
f[52] = 1/V5 * ( Q4*Z[3][0] + r[4][0]*V5 - Q5*Z[4][0] );
f[53] = 1/V5 * ( Q4*Z[3][1] + r[4][1]*V5 - Q5*Z[4][1] );
f[54] = 1/V5 * ( Q4*Z[3][2] + r[4][2]*V5 - Q5*Z[4][2] );
f[55] = 1/V5 * ( Q4*Z[3][3] + r[4][3]*V5 - Q5*Z[4][3] );
f[56] = 1/V5 * ( Q4*Z[3][4] + r[4][4]*V5 - Q5*Z[4][4] );
f[57] = 1/V5 * ( Q4*Z[3][5] + r[4][5]*V5 - Q5*Z[4][5] );
f[58] = 1/V5 * ( Q4*Z[3][6] + r[4][6]*V5 - Q5*Z[4][6] );
f[59] = 1/V5 * ( Q4*Z[3][7] + r[4][7]*V5 - Q5*Z[4][7] + KLa5*V5*(SO_star - Z[4][7]) );
f[60] = 1/V5 * ( Q4*Z[3][8] + r[4][8]*V5 - Q5*Z[4][8] );
f[61] = 1/V5 * ( Q4*Z[3][9] + r[4][9]*V5 - Q5*Z[4][9] );
f[62] = 1/V5 * ( Q4*Z[3][10] + r[4][10]*V5 - Q5*Z[4][10] );
f[63] = 1/V5 * ( Q4*Z[3][11] + r[4][11]*V5 - Q5*Z[4][11] );
f[64] = 1/V5 * ( Q4*Z[3][12] + r[4][12]*V5 - Q5*Z[4][12] );


//Secondary settler: function 66-145

//The solid flux (X) in settler: function 66-75
//From layer 1 to layer 10
X[0] = ZS[0][0];  // layer 1
X[1] = ZS[1][0]; 
X[2] = ZS[2][0]; 
X[3] = ZS[3][0]; 
X[4] = ZS[4][0];
X[5] = ZS[5][0]; 
X[6] = ZS[6][0]; 
X[7] = ZS[7][0]; 
X[8] = ZS[8][0]; 
X[9] = ZS[9][0]; // layer 10

Xmin = fns * Xf;
vdn = (Qr + Qw)/A;  
vup = Qe/A; 

//Js (layer 1 to layer 6)
for (int i = 0; i < 10; i++) {
	 expfun[i] = v0* ( Math.exp(-rh*(X[i] - Xmin)) - Math.exp(-rp* (X[i] - Xmin)) );
	 vs[i] = Math.max( 0, Math.min(v0_pie,expfun[i]) );
	 Js[i] = vs[i] * X[i];
}

//for (int i = 0; i < Js.length; i++) {
//	System.out.println(Js[i]);
//}

//Jclar (layer 7 to layer 9)
for (int j = 6; j < 9; j++) {
	if (X[j-1] > Xt) {
		Jclar[j] = Math.min( vs[j]*X[j], vs[j-1]*X[j-1] );
	} else {
		Jclar[j] = vs[j]*X[j];
	}
		
	}

//Jclar (layer 10)
if (X[8] > Xt) {
	Jclar[9] = Math.min( vs[9]*X[9], vs[8]*X[8] );
} else {
	Jclar[9] = vs[9]*X[9];
}

//for (int i = 0; i < Jclar.length; i++) {
//	System.out.println(Jclar[i]);
//}

//the solid flux in settler: form layer 1 to layer 10
f[65] = ( vdn*(X[1] - X[0]) + Math.min(Js[1],Js[0]) )/zm;
f[66] = ( vdn*(X[2] - X[1]) + Math.min(Js[1],Js[2]) - Math.min(Js[1],Js[0]) )/zm;
f[67] = ( vdn*(X[3] - X[2]) + Math.min(Js[2],Js[3]) - Math.min(Js[2],Js[1]) )/zm;
f[68] = ( vdn*(X[4] - X[3]) + Math.min(Js[3],Js[4]) - Math.min(Js[3],Js[2]) )/zm;
f[69] = ( vdn*(X[5] - X[4]) + Math.min(Js[4],Js[5]) - Math.min(Js[4],Js[3]) )/zm;
f[70] = ( Qf*Xf/A  + Jclar[6] - (vup + vdn)*X[5] - Math.min(Js[5],Js[4]) )/zm;
f[71] = ( vup*(X[5] - X[6]) + Jclar[7] - Jclar[6] )/zm;
f[72] = ( vup*(X[6] - X[7]) + Jclar[8] - Jclar[7] )/zm;
f[73] = ( vup*(X[7] - X[8]) + Jclar[9] - Jclar[8] )/zm;
f[74] = ( vup*(X[8] - X[9]) - Jclar[9] )/zm;

//the soluble components in settler:  function 76-145
//SI SS SO SNO SNH SND SALK

//SI: layer 1 to layer 10
f[75] = vdn*(ZS[1][1] - ZS[0][1])/zm;
f[76] = vdn*(ZS[2][1] - ZS[1][1])/zm;
f[77] = vdn*(ZS[3][1] - ZS[2][1])/zm;
f[78] = vdn*(ZS[4][1] - ZS[3][1])/zm;
f[79] = vdn*(ZS[5][1] - ZS[4][1])/zm;
f[80] = ( Qf*Z[4][0]/A - (vdn + vup)*ZS[5][1] )/zm;
f[81] = vup*(ZS[5][1] -ZS[6][1])/zm;
f[82] = vup*(ZS[6][1] -ZS[7][1])/zm;
f[83] = vup*(ZS[7][1] -ZS[8][1])/zm;
f[84] = vup*(ZS[8][1] -ZS[9][1])/zm;

//SS: layer 1 to layer 10
f[85] = vdn*(ZS[1][2] - ZS[0][2])/zm;
f[86] = vdn*(ZS[2][2] - ZS[1][2])/zm;
f[87] = vdn*(ZS[3][2] - ZS[2][2])/zm;
f[88] = vdn*(ZS[4][2] - ZS[3][2])/zm;
f[89] = vdn*(ZS[5][2] - ZS[4][2])/zm;
f[90] = ( Qf*Z[4][1]/A - (vdn + vup)*ZS[5][2] )/zm;
f[91] = vup*(ZS[5][2] -ZS[6][2])/zm;
f[92] = vup*(ZS[6][2] -ZS[7][2])/zm;
f[93] = vup*(ZS[7][2] -ZS[8][2])/zm;
f[94] = vup*(ZS[8][2] -ZS[9][2])/zm;

//SO: layer 1 to layer 10
f[95] = vdn*(ZS[1][3] - ZS[0][3])/zm;
f[96] = vdn*(ZS[2][3] - ZS[1][3])/zm;
f[97] = vdn*(ZS[3][3] - ZS[2][3])/zm;
f[98] = vdn*(ZS[4][3] - ZS[3][3])/zm;
f[99] = vdn*(ZS[5][3] - ZS[4][3])/zm;
f[100] = ( Qf*Z[4][7]/A - (vdn + vup)*ZS[5][3] )/zm;
f[101] = vup*(ZS[5][3] -ZS[6][3])/zm;
f[102] = vup*(ZS[6][3] -ZS[7][3])/zm;
f[103] = vup*(ZS[7][3] -ZS[8][3])/zm;
f[104] = vup*(ZS[8][3] -ZS[9][3])/zm;

//SNO: layer 1 to layer 10
f[105] = vdn*(ZS[1][4] - ZS[0][4])/zm;
f[106] = vdn*(ZS[2][4] - ZS[1][4])/zm;
f[107] = vdn*(ZS[3][4] - ZS[2][4])/zm;
f[108] = vdn*(ZS[4][4] - ZS[3][4])/zm;
f[109] = vdn*(ZS[5][4] - ZS[4][4])/zm;
f[110] = ( Qf*Z[4][8]/A - (vdn + vup)*ZS[5][4] )/zm;
f[111] = vup*(ZS[5][4] -ZS[6][4])/zm;
f[112] = vup*(ZS[6][4] -ZS[7][4])/zm;
f[113] = vup*(ZS[7][4] -ZS[8][4])/zm;
f[114] = vup*(ZS[8][4] -ZS[9][4])/zm;

//SNH: layer 1 to layer 10
f[115] = vdn*(ZS[1][5] - ZS[0][5])/zm;
f[116] = vdn*(ZS[2][5] - ZS[1][5])/zm;
f[117] = vdn*(ZS[3][5] - ZS[2][5])/zm;
f[118] = vdn*(ZS[4][5] - ZS[3][5])/zm;
f[119] = vdn*(ZS[5][5] - ZS[4][5])/zm;
f[120] = ( Qf*Z[4][9]/A - (vdn + vup)*ZS[5][5] )/zm;
f[121] = vup*(ZS[5][5] -ZS[6][5])/zm;
f[122] = vup*(ZS[6][5] -ZS[7][5])/zm;
f[123] = vup*(ZS[7][5] -ZS[8][5])/zm;
f[124] = vup*(ZS[8][5] -ZS[9][5])/zm;

//SND: layer 1 to layer 10
f[125] = vdn*(ZS[1][6] - ZS[0][6])/zm;
f[126] = vdn*(ZS[2][6] - ZS[1][6])/zm;
f[127] = vdn*(ZS[3][6] - ZS[2][6])/zm;
f[128] = vdn*(ZS[4][6] - ZS[3][6])/zm;
f[129] = vdn*(ZS[5][6] - ZS[4][6])/zm;
f[130] = ( Qf*Z[4][10]/A - (vdn + vup)*ZS[5][6] )/zm;
f[131] = vup*(ZS[5][6] -ZS[6][6])/zm;
f[132] = vup*(ZS[6][6] -ZS[7][6])/zm;
f[133] = vup*(ZS[7][6] -ZS[8][6])/zm;
f[134] = vup*(ZS[8][6] -ZS[9][6])/zm;

//SALK: layer 1 to layer 10
f[135] = vdn*(ZS[1][7] - ZS[0][7])/zm;
f[136] = vdn*(ZS[2][7] - ZS[1][7])/zm;
f[137] = vdn*(ZS[3][7] - ZS[2][7])/zm;
f[138] = vdn*(ZS[4][7] - ZS[3][7])/zm;
f[139] = vdn*(ZS[5][7] - ZS[4][7])/zm;
f[140] = ( Qf*Z[4][12]/A - (vdn + vup)*ZS[5][7] )/zm;
f[141] = vup*(ZS[5][7] -ZS[6][7])/zm;
f[142] = vup*(ZS[6][7] -ZS[7][7])/zm;
f[143] = vup*(ZS[7][7] -ZS[8][7])/zm;
f[144] = vup*(ZS[8][7] -ZS[9][7])/zm;

*/


//for (int i = 0; i < f.length; i++) {
//System.out.println(f[i]);
//
//}	
	return f;
	
	}
//----------------------------------------------------------------------------------------------------	    
/**
 * Euler Method: x(k-1)-->x(k)	    
 * @param x   x(k-1)
 * @param u   u(k)
 * @param dt  sample time
 * @return xt x(k)
 */
    public double[] intNomModel(double[] x, double[] Xk, double[] u, double dt, double Q0, double[] Z0) {
        
        double[] f = new double[nStates];
    	double[] xt = new double[nStates]; 
    	    	
    	System.arraycopy(x, 0, xt, 0, nStates);
        int M = (int) (Math.round(((dt * 1E4) / (h * 1E4))));
        
        for (int i = 0; i < M; i++) {
            f = nomVectorField(xt, Xk, u, Q0, Z0);
            for (int j = 0; j<nStates; j++){
            	xt[j] += h*f[j];
            }
        }
        
        return xt;
    }	    
   
//-------------------------------------------------------------------------------------------------------
 
    public double[] pid (double[] uk1, double[] yk1, double[] yk_1){
    	// uk_1: {u[0](k-1), u[1](k-1)}
    	
    	double[] du = new double[mInputs];
    	double[] uk = new double[mInputs];
    	
    /*	// so
    	double r_so = yset[0];
    	double K_so = 25;         // m3/d/(g N/m3);
    	double Ti_so = 0.002;     // days
    	double T_so = 0.001;
 
    	double ek_so = r_so - yk[0];     // e(k)
    	double ek_1_so = r_so - yk_1[0];   // e(k-1)

        du[0] = K_so*( (ek_so - ek_1_so) + T_so/Ti_so*ek_so );   // du = K*( (e(k)-e(k-1)) + T/Ti*e(k) );
    	uk[0] = uk_1[0] + du[0];
 */
    	
    	// sno
    	double r_sno = yset[1];
    	double K_sno = 10000;        // m3/d/(g N/m3);
    	double Ti_sno = 0.025;       // days
    	double T_sno = 0.015;        // days

    	double ek_sno = r_sno -yk1[0];       // e(k)
    	double ek_1_sno = r_sno - yk_1[0];    // e(k-1)

    	du[0] = K_sno*( (ek_sno - ek_1_sno) + T_sno/Ti_sno*ek_sno );     // du = K*( (e(k)-e(k-1)) + T/Ti*e(k) );
    	uk[0] = uk1[0] + du[0];
    	
    	return uk;
    }


//---------------------------------------------------------------------------------------------------------	 
//---------------------------------------------------------------------------------------------------------	    

    // vector .*
    public double[] dotm(double[] a, double[] b ){
    	double[] c = new double [a.length];
    	for (int i = 0; i < a.length; i++) {
    		 c[i] = a[i]*b[i];
		}
    	return c;
    }
//-----------------------------------------------------------------------------------------------------------
    // vector ./
    public double[] dotd(double[] a, double[] b ){
    	double[] c = new double [a.length];
    	for (int i = 0; i < a.length; i++) {
    		 c[i] = a[i]/b[i];
		}
    	return c;
    }
//-----------------------------------------------------------------------------------------------------------
    // b is the integral of vector a ; trapezoid
    public double trapz(double[] a){
       double b = 0;
       for (int i = 0; i < a.length-1; i++) {
		b = b + (a[i] + a[i+1])*Delta/2;
	}
    	return b;
    }
    
//-------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------------------------------------------------------------
    // stage cost about economic mpc
    public double stagecost_empc(double[] xk1, double[] uk, double Q0, double[] Xk){
    	double l;
    	double Qw = 385;
    	double Qr = 18446;
		double Qe = Q0 - Qw;
    	
    	double SNHe = Xk[124];
    	double SNDe = Xk[134];
    	double SSe = Xk[94];
    	double SIe = Xk[84];
    	double SNOe = Xk[114];
   
    	// XNDe XBHe XBAe XPe XIe XSe
    	double XI5 = Xk[54];
    	double XS5 = Xk[55];
    	double XBH5 = Xk[56];
    	double XBA5 = Xk[57];
    	double XP5 = Xk[58];
    	double XND5 = Xk[63];
    	double Xe = Xk[74];
    	double Xf = 0.75 * ( XI5 + XS5 + XBH5 + XBA5 + XP5 );
    	double XNDe = (Xe/Xf)*XND5;
    	double XBHe = (Xe/Xf)*XBH5;
		double XBAe = (Xe/Xf)*XBA5;
		double XPe = (Xe/Xf)*XP5;
		double XIe = (Xe/Xf)*XI5;
		double XSe = (Xe/Xf)*XS5;
		
    	// EQ --------- 
    	double SNkje = SNHe + SNDe + XNDe + iXB*( XBHe + XBAe ) + iXP*( XPe + XIe );
		double CODe = SSe + SIe + XSe + XIe + XBHe + XBAe + XPe;
		double BOD5e = 0.25 * ( SSe + XSe + (1-fP) * (XBHe + XBAe) );
		double TSSe = Xe;
		double KLa5 = 84; //estimated average  //uk[0];
		double Qa = uk[0];
		double MEk;
		
		double EQk = (2*TSSe + CODe + 30*SNkje + 10*(SNOe) + 2*BOD5e)* Qe /1000;
		

		// OCI -----------
	    double TSSak = 0.75* ( V1*( xk1[2] + xk1[3] + xk1[4] + xk1[5] + xk1[6] )+ 
			                   V2*( xk1[15] + xk1[16] + xk1[17] + xk1[18] + xk1[19] )+ 
			                   V3*( xk1[28] + xk1[29] + xk1[30] + xk1[31] + xk1[32] )+ 
			                   V4*( xk1[41] + xk1[42] + xk1[43] + xk1[44] + xk1[45] )+ 
			                   V5*( Xk[54] + Xk[55] + Xk[56] + Xk[57] + Xk[58] ) );
	    double TSSsk = zm*A*(Xk[65] + Xk[66] + Xk[67] + Xk[68] + Xk[69] + Xk[70] + Xk[71] + Xk[72] + Xk[73] + Xk[74]);
	    double TSSk = TSSak + TSSsk;
	    
	    double SPk = ( TSSk - TSS0  + Xk[65] * Qw) /1000;
		double PEk = 0.004*Qa + 0.008*Qr + 0.05*Qw;
		double AEk = SO_star/1.8/1000*(V3*KLa3 + V4*KLa4 + V5*KLa5);
		if (KLa5 < 20) {
			MEk = 24 * 0.005 * V5;
		} else {
            MEk = 0;
		}
		MEk = MEk + 24*0.005*(V1 + V2);
		
		double OCIk = AEk + PEk + 5*SPk + MEk;

		
		// stagecost--------
		
//		l = EQk/100000 ;      

		l = (EQk + OCIk*0.3)/100000; 
		
    	return l;
    }
  //-------------------------------------------------------------------------------------------------------------
    // evaluate objective function, used for IPOPT
    public double obj_eval_empc(double[] xk1, double[] uk1, int Nu, int Nh, double[] Q0_P, double[][] Z0_P, double[][] XX) {         
        
    	double cost = 0; 
    	double[] ukk = new double[mInputs];
    	double[] xkk = new double[nStates]; // to make the function pass-by-value
    	double[] ukk_1 = new double[mInputs]; // to make the function pass-by-value
    	double[] ykk = new double[mOutputs]; // to make the function pass-by-value
    	double[] ykk_1 = new double[mOutputs]; // to make the function pass-by-value
    	
    	double[] Xkk = new double[145]; // to make the function pass-by-value

    	double Q0;
    	double[] Z0 = new double[13];
    	
    	System.arraycopy(xk1, 0, xkk, 0, nStates);
    	
    	
    	
    for (int k = 0; k < Nu; k++) {
    	
       // ykk[0] = xkk[59];
        ykk[0] = xkk[21];
        
        System.arraycopy(XX[k], 0, Xkk, 0, 145);
        
        System.arraycopy(ykk, 0, ykk_1, 0, mOutputs);
        
    	System.arraycopy(uk1, k*mInputs, ukk, 0 , mInputs);
    	
    	Q0 = Q0_P[k];
    	Z0 = Z0_P[k];
    	cost = cost + stagecost_empc(xkk,ukk,Q0,Xkk); 
        xkk = intNomModel(xkk, Xkk, ukk, Delta, Q0, Z0);

    }
    
    // terminal cost
    System.arraycopy(ukk, 0, ukk_1, 0, mInputs);
	for (int k = 0; k < Nh; k++) {
       
        //ykk[0] = xkk[59];
        ykk[0] = xkk[21];
        
		ukk = pid(ukk_1, ykk, ykk_1);
                		
        Q0 = Q0_P[k + Nu];
    	Z0 = Z0_P[k + Nu];
    	cost = cost + stagecost_empc(xkk,ukk,Q0,Xkk); 
        xkk = intNomModel(xkk, Xkk, ukk, Delta, Q0, Z0);
    	
        System.arraycopy(ukk, 0, ukk_1, 0, mInputs);
        System.arraycopy(ykk, 0, ykk_1, 0, mOutputs);
	}

    return cost;
    	
}
    
//----------------------------------------------------------------------------------------------------------------	    

    // pid_eval used to calculate the initial value u0 of MPC  
	public double[] pid_eval(double[] xk1, double[] yk1, double[] uk1, int Nu, double[] Q0_P, double[][] Z0_P, double[][] XX ) {
		double[] u0 = new double[Nu*mInputs];
		double[] ukk = new double[mInputs];
		double[] ykk =  new double[mOutputs];
		
		double[] xkk = new double[nStates]; // to make the function pass-by-value
		double[] ykk_1 =  new double[mOutputs];
		double[] ukk_1 = new double[mInputs];
		double Q0;
    	double[] Z0 = new double[13];
    	double[] Xk = new double[145];
		
		System.arraycopy(xk1, 0, xkk, 0, nStates);
		System.arraycopy(yk1, 0, ykk_1, 0, mOutputs);
		System.arraycopy(uk1, 0, ukk_1, 0, mInputs);
		
		for (int k = 0; k < Nu; k++) {
	    	
	    	//ykk[0] = xkk[59];
	    	ykk[0] = xkk[21];
	    	
	        ukk = pid(ukk_1, ykk, ykk_1);
	        
	        Q0 = Q0_P[k];
        	Z0 = Z0_P[k];
	        xkk = intNomModel(xkk, XX[k], ukk, Delta, Q0, Z0);
	        
	        System.arraycopy(ukk, 0, u0, k*mInputs, mInputs);
	        System.arraycopy(ukk, 0, ukk_1, 0, mInputs);
	        System.arraycopy(ykk, 0, ykk_1, 0, mOutputs);
	        
	    }
		return u0;
	}
	
}
