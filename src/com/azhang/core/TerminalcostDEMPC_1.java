package com.azhang.core;

import org.coinor.Ipopt;

public class TerminalcostDEMPC_1 extends Ipopt {
	
	 /************************ Ipopt Problem Dimensions ************************/
   private int n;              // Number of decision variables
   private int m;              // Number of constraints
   private int nele_jac;       // Number of nonzeros in Jacobian of constraints
   private int nele_hess;      // Number of nonzeros in Hessian of Lagrangian
   /**************************************************************************/
   
   /**************** LEMPC Controller Variables and Parameters ***************/
   private double[] xk1;               // Current state
   private double[][] XX;                 // predicted trajectory from upstream neighbor   
   private double[] yk_1;
   private double[] uk_1;
   private double Delta;               // Sampling period
   private double h;           // Integration step
   private int Nu;                      // Control Prediction horizon
   private int Np;                      // Prediction horizon
   private int Nh;                  // terminal cost horizon

 
   //private boolean t0flag;             // Beg. op. period boolean
   private Subsystem1_Bsm1Model model;                // CSTR
   private double rho;                // Lyapunov-based constraint
   private double[] Q0_P;
   private double[][] Z0_P;
 


   
   /**************************************************************************/
   
   /**
    * Creates the LEMPC (Mode 1) object.
    * 
    * @param Nu Control Prediction horizon
    * @param Np Prediction horizon
    * @param Delta Sampling period
    * @param rhoe Bound on the Lyapunov function (Operation must be 
    *      maintained in {@latex.inline $\\Omega_{\\rho_e}$})
    * @param xtk Current state vector
    * @param xs Steady-state vector
    * @param us Steady-state input
    * @param sub1 BSM1 model
    */
   public TerminalcostDEMPC_1(int Nu, int Np, int Nh, double Delta, double h, double rho, double[] xk1,
           double[] yk_1, double[] uk_1, Subsystem1_Bsm1Model sub1, double[] Q0_P, double [][]Z0_P, double[][] XX) {
       
       // Control parameters
       this.Nu = Nu;                 // Control Prediction horizon
       this.Np = Np;                 // Prediction horizon
       this.Nh = Nh;
       this.Delta = Delta;         // Sampling period
       this.h=h;
       this.model = sub1;
       this.rho = rho;           // Omega_{rho} set
       this.xk1 = xk1;             // Current state
       this.yk_1 = yk_1;
       this.uk_1 = uk_1;
       this.Q0_P = Q0_P;
       this.Z0_P = Z0_P;
       this.XX = XX;
       
   }
   /**************************************************************************/

   /**
    * Callback function for the objective function.
    * 
    * @param n (in) Number of decision variables
    * @param u (in) Primal variable at which to evaluate the objective
    * @param new_u (in) False if any evaluation method was previously called 
    *      with the same values in u, true otherwise
    * @param obj_value (out) Objective function value
    * @return  true
    */
   @Override
   protected boolean eval_f(int n, double[] uk1, boolean new_u, 
           double[] obj_value) {
       
       assert n == this.n;
       
       obj_value[0] = model.obj_eval_empc(xk1, uk1, Nu, Nh, Q0_P, Z0_P, XX);
//       obj_value[0] = model.obj_eval(xtk, u, uk_1, Nu, Np, Nh);
       
       return true;
       
   }

   /**
    * Callback function for the gradient of the objective value. This method
    * approximates the gradient with a finite-difference method.
    * 
    * @param n (in) Number of decision variables
    * @param u (in) Primal variable at which to evaluate the gradient
    * @param new_u (in) False if any evaluation method was previously called 
    *      with the same values in x, true otherwise
    * @param grad_f (out) Array of values for the gradient of the objective
    *      function {@latex.inline $\\nabla f(x)$}
    * @return true
    */
   @Override
   protected boolean eval_grad_f(int n, double[] u, boolean new_u, 
       double[] grad_f) {
       
       assert n == this.n;
       
       double step = 1e-6;
       double xpstep[] = new double[n];
       double xmstep[] = new double[n];
       double objp[] = new double[1];
       double objm[] = new double[1];
       
       for (int i = 0; i < n; i++) {
           for (int j = 0; j < n; j++) {
               if (j == i) {
                   xpstep[j] = u[j] + step;
                   xmstep[j] = u[j] - step;
               } else {
                   xpstep[j] = u[j];
                   xmstep[j] = u[j];
               }
           }
           
           eval_f(n, xpstep, new_u, objp);
           eval_f(n, xmstep, new_u, objm);
           grad_f[i] = (objp[0] - objm[0]) / step / 2;
       }

       return true;
   }
   
  
   
   /**
    * Jacobian of the nonlinear constraints.
    * 
    * @param n (in) Number of decision variables
    * @param u (in) Primal variables
    * @param new_u (in) False, if any evaluation method was previously called
    *      with the same values of u, true otherwise
    * @param m (in) Number of constraints
    * @param nele_jac (in) Number of nonzeros elements in the Jacobian
    *      (dimension of iRow, jCol, and values)
    * @param iRow (out) Row indices of entries in the Jacobian of the 
    *      constraints
    * @param jCol (out) Column indices of entries in Jacobian of the 
    *      constraints
    * @param values (out) Values of the entries in Jacobian of the 
    *      constraints
    * @return true
    */
   @Override
   protected boolean eval_jac_g(int n, double[] u, boolean new_u,
           int m, int nele_jac, int[] iRow, int[] jCol, double[] values) {

       assert n == this.n;
       assert m == this.m;

       if (values == null) {
           // return the structure of the jacobian
           int cc = 0;
           for (int i = 0; i < m; i++) {
               for (int j = 0; j < n; j++) {
                   iRow[cc] = i;
                   jCol[cc] = j;
                   cc = cc + 1;
               }
           }
       } else {
           // return the values of the jacobian of the constraints
           double step = 1e-6;
           double xpstep[] = new double[n];
           double xmstep[] = new double[n];
           double objp[] = new double[m];
           double objm[] = new double[m];
           for (int i = 0; i < n; i++) {
               for (int j = 0; j < n; j++) {
                   if (j == i) {
                       xpstep[j] = u[j] + step;
                       xmstep[j] = u[j] - step;
                   } else {
                       xpstep[j] = u[j];
                       xmstep[j] = u[j];
                   }
               }
               eval_g(n, xpstep, new_u, m, objp);
               eval_g(n, xmstep, new_u, m, objm);
               for (int k = 0; k < m; k++) {
                   values[i + k * n] = (objp[k] - objm[k]) / step / 2;
               }
           }
       }
       return true;
   }

   /**
    * The Hessian of the Lagrangian function. This method returns false 
    * because we use the limited-memory Quasi-Newton method to approximate the 
    * Hessian.
    * 
    * @param n (in) Number of decision variables
    * @param u (in) Primal variables
    * @param new_u (in) False, if any evaluation method was previously called
    *      with the same values of u, true otherwise
    * @param obj_factor (in) Factor in from of the objective term in the 
    *      Hessian
    * @param m (in) Number of constraints
    * @param lambda (in) Constraint multipliers
    * @param new_lambda (in) False, if any evaluation method was previously 
    *      called with the same values of lambda, true otherwise
    * @param nele_hess (in) Number of nonzero elements in the Hessian
    *      (dimension of iRow, jCol, and values)
    * @param iRow (out) Row indices of entries in the Hessian
    * @param jCol (out) Column indices of entries in Hessian
    * @param values (out) Values of the entries in Hessian
    * @return false
    */
   @Override
   protected boolean eval_h(int n, double[] u, boolean new_u, 
           double obj_factor, int m, double[] lambda, boolean new_lambda,
           int nele_hess, int[] iRow, int[] jCol, double[] values) {
       return false;
   }
   /**************************************************************************/
   
//=====================================================================================================================   
   /**
    * Constraints of the LEMPC. 
    * 
    * @param n (in) Number of decision variables
    * @param u (in) Primal variables
    * @param new_u (in) False, if any evaluation method was previously called
    *   with the same values in x, true otherwise
    * @param m (in) Number of constraints
    * @param g (out) Array of constraint function values
    * @return true
    */
    @Override
    protected boolean eval_g(int n, double[] u, boolean new_u, int m, 
            double[] g) {
        
        assert n == this.n;
        assert m == this.m;
        
        double[] xkk = new double[model.nStates];
        double[] Xkk = new double[145];
        System.arraycopy(xk1, 0, xkk, 0, model.nStates);
        double[] ukk = new double[model.mInputs]; 
        double Q0;
	     double[] Z0 = new double[13];
        
        //double[] ukk_1 = new double[model.mInputs]; 
        //System.arraycopy(u, 0, ukk_1, 0, model.mInputs);
        
        for (int i = 0; i < Nu; i++) {
            
            // Get the control action
            System.arraycopy(u, i * model.mInputs, ukk, 0, model.mInputs);
            // Integrate the model forward
            Q0 = Q0_P[i];
	         Z0 = Z0_P[i];
    
            xkk = model.intNomModel(xkk, Xkk, ukk, Delta, Q0, Z0);
            g[i] = xkk[59]; // so
            g[Nu + i] = xkk[21]; // sno
            
//            g[i] = model.quadLyapFunction(xkk); //Omega_rho constraint guarantees that V_f and V_m are both positive
//            g[Nu+i] = model.Q_fd + ukk[2] - ukk[0] - ukk[1]; //Q_f
            
            //g[2*Nu+i] =  ukk[2] - ukk[0] - ukk[1] - (ukk_1[2] - ukk_1[0] - ukk_1[1]); //Q_f
//            System.arraycopy(ukk, 0, ukk_1, 0 , model.mInputs);
        }      
        return true;
    }
   
   /**
    * Compute the control action.
    * 
    * @param xtk State vector
    * @return Control action {@latex.inline $u(t_k)$}
    */
   	 public double[] ctrl(double[] xk1, double[] yk1, double[] uk1, int Nu, int Np, int Nh, double[] Q0_P, double [][] Z0_P, double[][] XX) {	
     	 
   	// Set the current state
			this.xk1 = xk1;
	    	this.Nu = Nu;
	    	this.Np = Np;
	    	this.Nh = Nh;
	    	this.Q0_P = Q0_P;
	    	this.Z0_P = Z0_P;
	    	this.XX = XX;
	    	
   	// System.out.println("Nu: "+ Nu);
   		// get an initial guess   
	    	
           double[] u0 = model.pid_eval(xk1, yk1, uk1, Nu, Q0_P, Z0_P, XX) ;
 
      	 	// Optimization problem size
           n = Nu * model.mInputs;
           m = Nu * 1 ;  // 2 is the number of constraints: so (xk[59]) and sno (xk[21]) 
           nele_jac = m * n;
           nele_hess = n * (n + 1) / 2;
       	
       	// Bounds on the optimization problem
           double u_L[] = new double[n];
           double u_U[] = new double[n];
           double g_L[] = new double[m];
           double g_U[] = new double[m];
           
   		double[] UP = {5*18446};         // input upper bound
   		double[] LP = {0};                 // input lower bound
   	   
   		//double[] UP = {240, 5*18446};         // input upper bound
   		//double[] LP = {0, 0};                 // input lower bound
           
           // Bound on the decision variable (square bound)
           for (int i = 0; i < Nu; i++) {
               for (int j = 0; j < model.mInputs; j++) {
                   u_L[i * model.mInputs + j] =   LP[j];
                   u_U[i * model.mInputs + j] =   UP[j];
               }
           }
           
           // Bound on the constraints
           for (int k = 0; k < Nu; k++) {
           	//g_L[k] = 150.0; //  V_f>150
               //g_U[k] = 250.0; // V_f<250
        	 
        	   g_L[k] = 0; // so bound, r_so = 2;
               g_U[k] = 10; // 
             //  g_L[Nu+k] = 0; //sno bound, r_sno = 1;
             //  g_U[Nu+k] = 10; 
               
             
               
               //g_L[2*Nu+k] = -1E-4; 
               //g_U[2*Nu+k] = 1E-4; 
           }

           // Create the Ipopt optimization problem
           int index_style = C_STYLE;
           create(n, u_L, u_U, m, g_L, g_U, nele_jac, nele_hess, index_style);
           
           // Depending on the verision could be addIntOption, 
           //      addStrOption, addNumOption
           addIntOption("print_level", 2);         // Print level
           addIntOption("max_iter", 1000);         // Maximum Interations
           addNumOption("tol", 1E-8);              // Overall tolerance
           addNumOption("dual_inf_tol", 1E-8);     // Dual tolerance
           addNumOption("acceptable_tol", 1E-8);   // Acceptable tolerance
           // Use limited-memory BFGS update for the Hessian
           addStrOption("hessian_approximation", "limited-memory");
       	
           // Save the new initial guess
       	//u02 = new double[Nu*model.mInputs];
           //System.arraycopy(u0, tau*model.mInputs, u02, 0, model.mInputs * Nu);
           //u0 = new double[Nu*model.mInputs];
           //System.arraycopy(u02, 0, u0, 0, model.mInputs * Nu);
       //}
   	
       // Solve the problem
		double[] u = solve(u0);
       //System.out.println("Ipopt Status = " + getStatus());
               
       // Copy the control action for t_k to t_(k+1)
       double[] utk = new double[model.mInputs];
       System.arraycopy(u, 0, utk, 0, model.mInputs);
    
       //return utk; 
       return utk; 
   }


   


}
